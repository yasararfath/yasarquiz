﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Trakomatic_WIFI_Yasar.ViewModel;
using System.Windows.Controls.Primitives;
using SimpleWifi;
using SimpleWifi.Win32;
using System.Threading.Tasks;

namespace Trakomatic_WIFI_Yasar
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			this.DataContext = new MainWindowViewModel();
		}

		private void ToggleButton_Click(object sender, RoutedEventArgs e)
		{
            // Toggle Button to Hide and show the Signal info
			DependencyObject obj = (DependencyObject)e.OriginalSource;
			while (!(obj is DataGridRow) && obj != null) obj = VisualTreeHelper.GetParent(obj);
			if (obj is DataGridRow)
			{
				if ((obj as DataGridRow).DetailsVisibility == Visibility.Visible)
				{					
					(obj as DataGridRow).IsSelected = false;
                }
				else
				{					
					(obj as DataGridRow).IsSelected = true;
				}
			}
		}

		private void dataGrid1_RowDetailsVisibilityChanged(object sender, DataGridRowDetailsEventArgs e)
		{
            // Toggle Button to Hide and show the Signal info
            DataGridRow row = e.Row as DataGridRow;
			FrameworkElement tb = GetTemplateChildByName(row, "RowHeaderToggleButton");
			if (tb != null)
			{
				if (row.DetailsVisibility == System.Windows.Visibility.Visible)
				{
					(tb as ToggleButton).IsChecked = true;
				}
				else
				{
					(tb as ToggleButton).IsChecked = false;
				}
			}

		}
		public static FrameworkElement GetTemplateChildByName(DependencyObject parent, string name)
		{
			int childnum = VisualTreeHelper.GetChildrenCount(parent);
			for (int i = 0; i < childnum; i++)
			{
				var child = VisualTreeHelper.GetChild(parent, i);
				if (child is FrameworkElement &&

						((FrameworkElement)child).Name == name)
				{
					return child as FrameworkElement;
				}
				else
				{
					var s = GetTemplateChildByName(child, name);
					if (s != null)
						return s;
				}
			}
			return null;
		}

		private void dataGrid1_Loaded(object sender, RoutedEventArgs e)
		{
            // Load to grid 
            DataGrid dg = sender as DataGrid;
			Border border = VisualTreeHelper.GetChild(dg, 0) as Border;
			ScrollViewer scrollViewer = VisualTreeHelper.GetChild(border, 0) as ScrollViewer;
			Grid grid = VisualTreeHelper.GetChild(scrollViewer, 0) as Grid;
			Button button = VisualTreeHelper.GetChild(grid, 0) as Button;

			if (button != null && button.Command != null && button.Command == DataGrid.SelectAllCommand)
			{
				button.IsEnabled = false;
				button.Opacity = 0;
			}
		}

        static string InPwd;

        private void CoolButton_Click(object sender, RoutedEventArgs e)
        {
            // Let's show our passwordbox.
       //     InputBox.Visibility = System.Windows.Visibility.Visible;
        }

        private void YesButton_Click(object sender, RoutedEventArgs e)
        {
            // Yes Button Clicked! Let's hide our InputBox and handle the input text.
            InputBox.Visibility = System.Windows.Visibility.Collapsed;

            // Do something with the Input
            String input = InputTextBox.Text;
            InPwd = input;
          //  MyListBox.Items.Add(input); // Add Input to our ListBox.

            // Clear InputBox.
            InputTextBox.Text = String.Empty;
            InputBox.Visibility = System.Windows.Visibility.Collapsed;
            ConnectWifi();
        }

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            // NoButton Clicked! Let's hide our InputBox.
            InputBox.Visibility = System.Windows.Visibility.Collapsed;

            // Clear InputBox.
            InputTextBox.Text = String.Empty;
        }
        

        private void OKButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
          
            MessageBox.Show(Convert.ToString (SelectedRows));
        }

        static int SelectedRows;
      
        private static Wifi wifi;
        //Function to Scan available wifi network
        public void scanWifi()
        {
            // Init wifi object and event handlers
            wifi = new Wifi();
            wifi.ConnectionStatusChanged += wifi_ConnectionStatusChanged;

            if (wifi.NoWifiAvailable)
            {
                MessageBox.Show("NO WIFI CARD WAS FOUND ");
            }
            else
            {
                List();
            }

        }
        //Function to Connect to a selected Wifi network
        public void ConnectWifi()
        {
            //Function to Connect to a selected Wifi network
            wifi.Disconnect();//Initialy disconnect any connected wifi to avoid timeout
            var accessPoints = List();

           //Getting the network index to be connected
            int selectedIndex = SelectedRows;
            if (selectedIndex > accessPoints.ToArray().Length || accessPoints.ToArray().Length == 0)
            {
             MessageBox.Show("Index out of bounds");
                return;
            }
            AccessPoint selectedAP = accessPoints.ToList()[selectedIndex];
            // AuthRequest for password
            AuthRequest authRequest = new AuthRequest(selectedAP);
            bool overwrite = true;

            if (authRequest.IsPasswordRequired)
            {
                authRequest.Password = PasswordPrompt(selectedAP);
            }
            // Connect with the Network
            selectedAP.ConnectAsync(authRequest, overwrite, OnConnectedComplete);
            
        }

        //--Disconnect the current connected wifi
        private void Disconnect()
        {
            //--Disconnect the current connected wifi
            wifi = new Wifi();
            wifi.Disconnect();
            if (wifi.ConnectionStatus == WifiStatus.Connected)
            {   
                MessageBox.Show("Connected");
            }
            else
            {
                MessageBox.Show("Disconnected");
            }
        }
        //Function to check Wifi network Status
        private void Status()
        {
            //Check the connection status
            if (wifi.ConnectionStatus == WifiStatus.Connected)
            {   
                MessageBox.Show("Connected by Status");   
            }
            else
            {
                MessageBox.Show("Unable to Connect");

            }
        }
        //Function to get the list of Wifi network 
        public IEnumerable<AccessPoint> List()
        {
            //Getting the list of Wifi Networks SSID , NETWORK SIGNAL, Connection Type 
            IEnumerable<AccessPoint> accessPoints = wifi.GetAccessPoints().OrderByDescending(ap => ap.SignalStrength);
            int i = 0;
            foreach (AccessPoint ap in accessPoints)
            {
                i++;
            }
            return accessPoints;
        }

      //Authenticate the Password
        private string PasswordPrompt(AccessPoint selectedAP)
        {
            string password = string.Empty;
            bool validPassFormat = false;
            while (!validPassFormat)
            {
                //Getting Wifi Password ");
                password = InPwd;

                validPassFormat = selectedAP.IsValidPassword(password);

                if (!validPassFormat)
                {
                    MessageBox.Show("Password is not valid for this network type.");

                    return password;
                }
            }

            return password;
        }
        //Network ProfileXML
        private void ProfileXML()
        {
            //Here i dont want to change the current profile for test demo - Yasar
            var accessPoints = List();
            MessageBox.Show("Enter the index of the network you wish to print XML for: ");

            int selectedIndex = int.Parse(Console.ReadLine());
            if (selectedIndex > accessPoints.ToArray().Length || accessPoints.ToArray().Length == 0)
            {
                MessageBox.Show("Index out of bounds");
                return;
            }
            AccessPoint selectedAP = accessPoints.ToList()[selectedIndex];

          //  MessageBox.Show("selectedAP.GetProfileXML());
        }
        //Delete Available Network Profile
        private void DeleteProfile()
        {
            // Function To delete the Current wifi profile---
            var accessPoints = List();
            MessageBox.Show("Enter the index of the network you wish to delete the profile: ");

            int selectedIndex = int.Parse(Console.ReadLine());
            if (selectedIndex > accessPoints.ToArray().Length || accessPoints.ToArray().Length == 0)
            {
                MessageBox.Show("Index out of bounds");
                return;
            }
            AccessPoint selectedAP = accessPoints.ToList()[selectedIndex];

            selectedAP.DeleteProfile();
            MessageBox.Show("Deleted profile for:" + selectedAP.Name);
        }

        //Function to Show network info
        private void ShowInfo()
        {
           // Function To show the current wifi network info
            var accessPoints = List();
            MessageBox.Show("Enter the index of the network you wish to see info about: ");

            int selectedIndex = int.Parse(Console.ReadLine());
            if (selectedIndex > accessPoints.ToArray().Length || accessPoints.ToArray().Length == 0)
            {
                MessageBox.Show("Index out of bounds");
                return;
            }
            AccessPoint selectedAP = accessPoints.ToList()[selectedIndex];

            MessageBox.Show(selectedAP.ToString());
        }

        public void wifi_ConnectionStatusChanged(object sender, WifiStatusEventArgs e)
        {
          //  MessageBox.Show(e.NewStatus.ToString());
        }
        static int ConStatus;
        public void OnConnectedComplete(bool success)
        {
            if (success == true)
            {
                ConStatus = 1;
                MessageBox.Show("Connected");
            }
            else
            {
                ConStatus = 0;
                MessageBox.Show("Not Connected");
            } 
        }

        private void dataGrid1_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (MessageBox.Show("Connect ?", "Hi..", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                DataGrid dg = sender as DataGrid;
                SelectedRows = dg.SelectedIndex;
           //     MessageBox.Show(Convert.ToString(dg.SelectedIndex) + "   Yo clicked me");
                scanWifi();
                InputBox.Visibility = Visibility.Visible;
            }
        }

        private void dataGrid1_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (MessageBox.Show("Disconnect ?", "Hi..", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)

            {
                Disconnect();
            }
        }
    }
}
