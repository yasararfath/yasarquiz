﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Trakomatic_WIFI_Yasar.Model;
using System.Windows.Controls;
using SimpleWifi;
using SimpleWifi.Win32;
//Developer Yasar Arfath Nowshath//
namespace Trakomatic_WIFI_Yasar.ViewModel
{
	public class MainWindowViewModel : INotifyPropertyChanged
	{
		public MainWindowViewModel()
		{
          
            WifiInfoCollection = new ObservableCollection<NetworkInfo>();
			BindData(); // Function To bind Wifi data to Grid
        }

		private ObservableCollection<NetworkInfo> wifiInfoCollection;
		public ObservableCollection<NetworkInfo> WifiInfoCollection
        {
			get { return wifiInfoCollection; }
			set
			{
                wifiInfoCollection = value;
				RaisePropertyChanged("WifiInfoCollection");
            }
		}

        public void wifi_ConnectionStatusChanged(object sender, WifiStatusEventArgs e)
        {
     //       Console.WriteLine("\nNew status: {0}", e.NewStatus.ToString());
        }
        private void BindData()
		{
         
            wifi = new Wifi();
            wifi.ConnectionStatusChanged += wifi_ConnectionStatusChanged;
            //Check for Wifi Caard available
            if (wifi.NoWifiAvailable)
            {
            //" NO WIFI CARD WAS FOUND ";
            }
            else
            {
                List();
            }


        }

        private static Wifi wifi;
        public IEnumerable<AccessPoint> List()
        {
            IEnumerable<AccessPoint> accessPoints = wifi.GetAccessPoints().OrderByDescending(ap => ap.SignalStrength);
          //--Adding Available Wifi info to Grid----
            int i = 0;
            foreach (AccessPoint ap in accessPoints)
            {
                i++;
                WifiInfoCollection.Add(new NetworkInfo(Convert.ToString (i), ap.Name, Convert.ToString(ap.SignalStrength) + "%") { Description = "Signal Strength :" + Convert.ToString(ap.SignalStrength)   + "% \nSecure: " + Convert.ToString(ap.IsSecure) });
            }
            return accessPoints;
        }


        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
		public void RaisePropertyChanged(string propertyName)
		{
			if (null != PropertyChanged)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion
	}
}
