﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//Developer Yasar Arfath Nowshath//
namespace Trakomatic_WIFI_Yasar.Model
{
	public class NetworkInfo
    {
        // Model for get set 
		public NetworkInfo(string sno,string ssid, string signal)
		{
            Sno = sno;
            SSID = ssid;
            Signal = signal;
        }
		public string Sno { get; set; }
		public string SSID { get; set; }
		public string Signal { get; set; }
		public string Connected { get; set; }
        public string IsSecure { get; set; }
        public string Description { get; set; }
            }
}
